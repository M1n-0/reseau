# tp5

## I. First steps

steam (1)
```
ip : 23.33.90.34
Port local : 58643
Port du serveur : 443
```
teams (2)
```
ip : 20.50.73.11
Port local: 58777
Port du serveur : 443
```

minecraft (3)
```
ip : 40.126.32.72
Port local: 58707
Port du serveur : 443
```

Whatsapp (4)
```
ip : 185.60.219.61
Port local : 58620
Port du serveur : 443
```

### OS
---

Steam :
```
PS C:\WINDOWS\system32> netstat -n -b | Select-String "92.122.218" -Context 0,1

>   TCP    10.33.70.190:59340     92.122.218.96:443      ESTABLISHED
   [steamwebhelper.exe]
>   TCP    10.33.70.190:59343     92.122.218.73:443      ESTABLISHED
   [steamwebhelper.exe]
```

Teams :
```
PS C:\WINDOWS\system32> netstat -n -b | Select-String "52.114.75" -Context 0,1

>   TCP    10.33.70.190:58783     52.114.75.169:443      ESTABLISHED
   [Teams.exe]
```

Minecraft :
```
PS C:\WINDOWS\system32> netstat -n -b | Select-String "52.237.134" -Context 0,1

>   TCP    10.33.70.190:59304     52.237.134.81:443      ESTABLISHED
   [GameBarPresenceWriter.exe]
>   TCP    10.33.70.190:59305     52.237.134.81:443      ESTABLISHED
   [GameBarPresenceWriter.exe]
```

Whatsapp :
```
PS C:\WINDOWS\system32> netstat -n -b | Select-String "185.60.219" -Context 0,1

>   TCP    10.33.70.190:59256     185.60.219.61:80       TIME_WAIT
    TCP    10.33.70.190:59271     20.42.73.24:443        ESTABLISHED
>   TCP    10.33.70.190:59319     185.60.219.61:80       ESTABLISHED
   [WhatsApp.exe]
```

## II. Setup Virtuel

### 1. SSH

```
ssh utilise TCP
```

```
1	0.000000	10.5.1.1	10.5.1.11	TCP	66	59538 → 22 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM

2	0.000172	10.5.1.11	10.5.1.1	TCP	66	22 → 59538 [SYN, ACK] Seq=0 Ack=1 Win=64240 Len=0 MSS=1460 SACK_PERM WS=128

3	0.000340	10.5.1.1	10.5.1.11	TCP	54	59538 → 22 [ACK] Seq=1 Ack=1 Win=262656 Len=0
```

```
77	11.355864	10.5.1.1	10.5.1.11	TCP	54	59538 → 22 [FIN, ACK] Seq=2542 Ack=3014 Win=2096384 Len=0
```

```
[nino@node1 ~]$ ss -tpn
State     Recv-Q     Send-Q           Local Address:Port           Peer Address:Port      Process
ESTAB     0          0                    10.5.1.11:22                 10.5.1.1:59461
```
```
PS C:\WINDOWS\system32> netstat -n -b | Select-String "ssh" -Context 1,0

    TCP    10.5.1.1:59461         10.5.1.11:22           ESTABLISHED
>  [ssh.exe]
```

### 2. Routage

```
[nino@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=24.6 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 24.576/24.576/24.576/0.000 ms
```

node1.tp5.b1 peut résoudre des noms de domaine publics (comme www.ynov.com)

```
[nino@node1 ~]$ ping google.com
PING google.com (172.217.20.206) 56(84) bytes of data.
64 bytes from par10s50-in-f14.1e100.net (172.217.20.206): icmp_seq=1 ttl=115 time=30.2 ms
64 bytes from par10s50-in-f14.1e100.net (172.217.20.206): icmp_seq=2 ttl=115 time=23.2 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1005ms
rtt min/avg/max/mdev = 23.248/26.725/30.202/3.477 ms
```

```
[nino@web ~]$ sudo dnf install ngix -y
```

```
[nino@web ~]$ cd /var/
[nino@web var]$ cd www/
-bash: cd: www/: No such file or directory
[nino@web var]$ sudo mkdir www
[nino@web var]$ cd www/
[nino@web www]$ sudo mkdir site_web_nul
[nino@web www]$ cd site_web_nul/
[nino@web site_web_nul]$ sudo touch index.html
[nino@web site_web_nul]$ sudo nano index.html
<h1>MEOW</h1>
```

```
[nino@web site_web_nul]$ sudo chown -R nginx:nginx /var/www/site_web_nul
[nino@web site_web_nul]$ ls -l
total 4
-rw-r--r--. 1 nginx nginx 14 Nov 14 11:31 index.html
```

```
[nino@web ~]$ cd /etc/nginx/conf.d/
[nino@web conf.d]$ sudo touch site_web_nul.conf
[nino@web conf.d]$ sudo nano site_web_nul.conf

server {
  # le port sur lequel on veut écouter
  listen 80;

  # le nom de la page d'accueil si le client de la précise pas
  index index.html;

  # un nom pour notre serveur
  server_name www.site_web_nul.b1;

  # le dossier qui contient notre site web
  root /var/www/site_web_nul;
}
```

```
[nino@web ~]$ sudo systemctl start nginx
[nino@web ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-11-14 11:36:02 CET; 32s ago
    Process: 1194 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1195 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1196 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1197 (nginx)
      Tasks: 2 (limit: 5896)
     Memory: 1.9M
        CPU: 13ms
     CGroup: /system.slice/nginx.service
             ├─1197 "nginx: master process /usr/sbin/nginx"
             └─1198 "nginx: worker process"

Nov 14 11:36:02 web.tp5.b1 systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 14 11:36:02 web.tp5.b1 nginx[1195]: nginx: the configuration file /etc/nginx/nginx.conf syntax is>
Nov 14 11:36:02 web.tp5.b1 nginx[1195]: nginx: configuration file /etc/nginx/nginx.conf test is succe>
Nov 14 11:36:02 web.tp5.b1 systemd[1]: Started The nginx HTTP and reverse proxy server.
lines 1-18/18 (END)
```

```
[nino@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[nino@web ~]$ sudo firewall-cmd --reload
success
[nino@web ~]$
```

```
[nino@node1 ~]$ curl 10.5.1.12:80
<h1>MEOW</h1>
```

```
[nino@node1 ~]$ sudo ss -l -t -n
[sudo] password for nino:
State      Recv-Q     Send-Q           Local Address:Port           Peer Address:Port     Process
LISTEN     0          128                    0.0.0.0:22                  0.0.0.0:*
LISTEN     0          128                       [::]:22                     [::]:*
```
